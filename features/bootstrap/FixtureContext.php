<?php


use Behat\Behat\Context\Context;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Fidry\AliceDataFixtures\LoaderInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\EntityManager;

class FixtureContext implements Context
{
    /** @var LoaderInterface */
    private $loader;

    /** @var string */
    private $fixturesBasePath;

    /**
     * @var array Will contain all fixtures in an array with the fixture
     *            references as key
     */
    private $fixtures;

    public function __construct(
        Registry $doctrine,
        LoaderInterface $loader,
        string $fixturesBasePath
    )
    {
        $this->loader = $loader;
        $this->fixturesBasePath = $fixturesBasePath;
        $this->doctrine=$doctrine;

    }

    /**
     * @Given the fixtures file :fixturesFile is loaded
     */
    public function theFixturesFileIsLoaded(string $fixturesFile): void
    {
        /** @var ObjectManager[] $managers */
        $managers = $this->doctrine->getManagers();
        foreach ($managers as $manager) {
            if ($manager instanceof EntityManager) {
                $schemaTool = new SchemaTool($manager);
                $schemaTool->dropDatabase();
                $schemaTool->createSchema($manager->getMetadataFactory()->getAllMetadata());
            }
        }
        $objectSet= $this->loader->load([$this->fixturesBasePath . $fixturesFile]);
        foreach ($managers as $manager) {
            $manager->clear();
        }

    }


}