<?php

use Behat\Gherkin\Node\TableNode;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Behat\Context\TranslatableContext;
use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Driver\BrowserKitDriver;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext extends BehatBaseContext implements TranslatableContext
{



    /**
     * @var Response|null
     */
    private $response;


    /**
     * Initializes context.
     */
    public function __construct()
    {
    }

    /**
     * @Then I am redirected to login page
     */
    public function iAmRedirectedToLoginPage()
    {
        $this->assertUrlRegExp('/\/login$/m');
    }

    /**
     * @Then /^I should see a table with "([^"]*)" in the "([^"]*)" column$/
     */
    public function iShouldSeeATableWithInTheNamedColumn($list, $column)
    {
        $cells = array_merge(array($column), $this->getFormater()->listToArray($list));
        $expected = new TableNode(array_map(function ($cell) {
            return [$cell];
        }, $cells));

        $this->iShouldSeeTheFollowingTable($expected);
    }

    /**
     * @Given /^I should see the following table:?$/
     */
    public function iShouldSeeTheFollowingTable($expected)
    {
        if ($expected instanceof TableNode) {
            $expected = $expected->getTable();
        }
        foreach ($expected as $i => $row) {
            foreach ($row as $j => $cell) {
                if (substr($cell, 0, 5) == '<now(') {
                    $format = substr($cell, 6, -3);
                    $fmt = new \IntlDateFormatter("it_IT", NULL, NULL);
                    $fmt->setPattern($format);
                    $expected[$i][$j] = $fmt->format(time());
                }
            }
        }

        $this->iShouldSeeATable();
        $tables = $this->findTables();
        $exceptions = array();

        foreach ($tables as $table) {
            try {
                if (false === $extraction = $this->extractColumns(current($expected), $table)) {
                    $this->getAsserter()->assertArrayEquals($expected, $table, true);
                    return;
                }
                $this->getAsserter()->assertArrayEquals($expected, $extraction, true);

                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) {
            return $e->getMessage();
        }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see the following table portion:?$/
     */
    public function iShouldSeeTheFollowingTablePortion($expected)
    {
        if ($expected instanceof TableNode) {
            $expected = $this->reorderArrayKeys($expected->getTable());
        }

        $this->iShouldSeeATable();

        $tables = $this->findTables();
        $exceptions = array();
        foreach ($tables as $table) {
            $extraction = $this->extractColumns(current($expected), $table);
            $this->getAsserter()->assertArrayContains($expected, $table);
            try {
                if (false === $extraction = $this->extractColumns(current($expected), $table)) {
                    $this->getAsserter()->assertArrayContains($expected, $table);

                    return;
                }
                $this->getAsserter()->assertArrayContains($expected, $extraction);

                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) {
            return $e->getMessage();
        }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see a table with ([^"]*) rows$/
     */
    public function iShouldSeeATableWithRows($nbr)
    {
        $nbr = (int)$nbr;

        $this->iShouldSeeATable();
        $exceptions = array();
        $tables = $this->getSession()->getPage()->findAll('css', 'table');

        foreach ($tables as $table) {
            try {
                if (null !== $body = $table->find('css', 'tbody')) {
                    $table = $body;
                }
                $rows = $table->findAll('css', 'tr');
                $this->getAsserter()->assertEquals($nbr, count($rows), sprintf('Table with %s rows expected, table with %s rows found.', $nbr, count($rows)));
                return;
            } catch (\Exception $e) {
                $exceptions[] = $e;
            }
        }

        $message = implode("\n", array_map(function ($e) {
            return $e->getMessage();
        }, $exceptions));

        throw new \Exception($message);
    }

    /**
     * @Then /^I should see a table$/
     */
    public function iShouldSeeATable()
    {
        $this->getSession()->wait(1000, "document.readyState === 'complete'");
        try {
            $this->getSession()->wait(2000, '0 < document.getElementsByTagName("TABLE").length');
        } catch (\Exception $ex) {
            unset($ex);
        }
        $tables = $this->getSession()->getPage()->findAll('css', 'table');
        $this->getAsserter()->assert(0 < count($tables), 'No table found');
    }

    protected function extractColumns(array $headers, array $table)
    {
        if (0 == count($table) || 0 == count($headers)) {
            return false;
        }

        $columns = array();
        $tableHeaders = current($table);
        foreach ($headers as $header) {
            $inArray = false;
            foreach ($tableHeaders as $index => $thead) {
                if ($thead === $header) {
                    $columns[] = $index;
                    $inArray = true;
                }
            }
            if (false === $inArray) {
                return false;
            }
        }

        $result = array();
        foreach ($table as $row) {
            $node = array();
            foreach ($row as $index => $value) {
                if (in_array($index, $columns)) {
                    $node[] = $value;
                }
            }
            $result[] = $node;
        }

        return $result;
    }

    protected function findTables()
    {
        $this->getSession()->wait(1000, "document.readyState === 'complete'");
        try {
            $this->getSession()->wait(2000, '0 < document.getElementsByTagName("TABLE").length');
        } catch (\Exception $ex) {
            unset($ex);
        }
        $tables = $this->getSession()->getPage()->findAll('css', 'table');

        foreach ($tables as $table) {
            $node = array();
            $head = $table->find('css', 'thead');
            $body = $table->find('css', 'tbody');
            $foot = $table->find('css', 'tfoot');
            if (null !== $head || null !== $body || null !== $foot) {
                $this->extractDataFromPart($head, $node);
                $this->extractDataFromPart($body, $node);
                $this->extractDataFromPart($foot, $node);
            } else {
                $this->extractDataFromPart($table, $node);
            }
            $result[] = $node;
        }
        return $result;
    }

    protected function extractDataFromPart($part, &$array)
    {
        if (null === $part) {

            return;
        }

        foreach ($part->findAll('css', 'tr') as $row) {
            $array[] = $this->extractDataFromRow($row);
        }
    }

    protected function extractDataFromRow($row)
    {
        $result = array();
        $elements = array_merge($row->findAll('css', 'th'), $row->findAll('css', 'td'));

        foreach ($elements as $element) {
            $result[] = preg_replace('!\s+!', ' ', $element->getText());
        }

        return $result;
    }

    protected function reorderArrayKeys(array $subject)
    {
        $orderedArray = array();

        foreach ($subject as $key => $value) {
            if (is_int($key)) {
                $orderedArray[] = $value;
            } else {
                $orderedArray[$key] = $value;
            }
        }

        return $orderedArray;
    }

    protected function getAsserter()
    {
        return new Asserter($this->getFormater());
    }

    protected function getFormater()
    {
        return new TextFormater;
    }



    /************************* new context ********************/
    /**
     * @Given I log in as :username
     */
    public function iLogInAs($username)
    {
        $this->visitPath('/login');
        $this->fillField('username', $username);
        $this->fillField('password', $username);
        $this->pressButton('Entra');
    }

    /**
     * Checks, that the specified element contains the specified text. When running Javascript tests it also considers that texts may be hidden.
     *
     * @Then /^I should see "(?P<text_string>(?:[^"]|\\")*)" in the "(?P<element_string>(?:[^"]|\\")*)" region$/
     * @param string $text
     * @param string $element Element we look in.
     * @throws ElementNotFoundException
     * @throws ExpectationException
     */
    public function assert_element_contains_text($text, $region)
    {

        $items = $this->getSession()->getPage()->findAll('xpath', "//div[@id='$region-region']");

        foreach ($items as $item) {
            $elementText = preg_replace('!\s+!', ' ', $item->getText());
            if (strpos($elementText, $text) !== false) {
                return;
            }
        }

        throw new ExpectationException('No text "' . $text . '" found in region "' . $region . '"', $this->getSession()->getDriver());

    }

    /**
     * Checks, that the specified element contains the specified text. When running Javascript tests it also considers that texts may be hidden.
     *
     * @Then /^I should not see "(?P<text_string>(?:[^"]|\\")*)" in the "(?P<element_string>(?:[^"]|\\")*)" region$/
     * @param string $text
     * @param string $element Element we look in.
     * @throws ElementNotFoundException
     * @throws ExpectationException
     */
    public function assert_element_not_contains_text($text, $region)
    {

        $items = $this->getSession()->getPage()->findAll('xpath', "//div[@id='$region-region']");

        foreach ($items as $item) {
            $elementText = preg_replace('!\s+!', ' ', $item->getText());
            if (strpos($elementText, $text) !== false) {
                throw new ExpectationException('Text "' . $text . '" found in region "' . $region . '"', $this->getSession()->getDriver());

            }
        }


    }

    /**
     * @Then I log out
     */
    public function iLogOut()
    {
        $this->visitPath('/logout');
    }

    /**
     * @Given I am on site homepage
     */
    public function iAmOnSiteHomepage()
    {
        $this->visitPath('/');


    }


    /**
     * Click on the element of the specified type which is located inside the second element.
     *
     * @When /^I click on "(?P<element_string>(?:[^"]|\\")*)" "(?P<selector_string>[^"]*)" in the "(?P<element_container_string>(?:[^"]|\\")*)" "(?P<text_selector_string>[^"]*)"$/
     * @param string $element Element we look for
     * @param string $selectortype The type of what we look for
     * @param string $nodeelement Element we look in
     * @param string $nodeselectortype The type of selector where we look in
     */
    public function i_click_on_in_the($element, $selectortype, $nodeelement, $nodeselectortype)
    {

        $node = $this->get_node_in_container($selectortype, $element, $nodeselectortype, $nodeelement);
        $this->ensure_node_is_visible($node);
        $node->click();
    }


    /**
     * @Then I should see a :link link
     */
    public function iShouldSeeALink($link)
    {

        $items = $this->getSession()->getPage()->findAll('xpath', '//a[text()[normalize-space(.) = "' . $link . '"]]');
        if (count($items) == 0) {
            throw new ExpectationException('No link with text "' . $link . '" found "', $this->getSession()->getDriver());
        }
    }


    /**
     * @Then I should not see a :link link
     */
    public function iShouldNotSeeALink($link)
    {
        $items = $this->getSession()->getPage()->findAll('xpath', '//a[text()[normalize-space(.) = "' . $link . '"]]');
        if (count($items) != 0) {
            throw new ExpectationException('Link with text "' . $link . '" found "', $this->getSession()->getDriver());
        }
    }

    /**
     * @Given /^I am authenticated as "([^"]*)"$/
     */
    public function iAmAuthenticatedAs($username)
    {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof BrowserKitDriver) {
            throw new UnsupportedDriverActionException('This step is only supported by the BrowserKitDriver');
        }

        $client = $driver->getClient();
        $client->getCookieJar()->set(new Cookie(session_name(), true));

        $session = $client->getContainer()->get('session');

        $user = $this->kernel->getContainer()->get('fos_user.user_manager')->findUserByUsername($username);
        $providerKey = $this->kernel->getContainer()->getParameter('fos_user.firewall_name');

        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
        $session->set('_security_' . $providerKey, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }


    /**
     * @When I accept alert
     */
    public function iAcceptAlert()
    {
        $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
    }


    /**
     * @When I click :arg1 checkbox
     */
    public function iClickCheckbox($label)
    {

        $nodes = $this->getSession()->getPage()->findAll('xpath', '//label[text()[normalize-space(.) ="' . $label . '"]]/..');

        if (count($nodes) == 0) {
            throw new ExpectationException('No checkbox with text "' . $label . '" found "', $this->getSession()->getDriver());
        }
        foreach ($nodes as $node) {
            //$this->ensure_node_is_visible($node);
            $node->click();
        }

    }


    /**
     * @Given I press :label and press :choice in select
     */
    public function iPressAndPressInSelect($label, $choice)
    {
        $label = $this->fixStepArgument($label);
        $this->getSession()->wait(1000, "document.readyState === 'complete'");
        $nodes = $this->getSession()->getPage()->findAll('xpath',  '//button[@title[normalize-space(.) ="' . $label . '"]]');
        if (count($nodes) == 0) {
            throw new ExpectationException('No select with text "' . $label . '" found "', $this->getSession()->getDriver());
        }
        foreach ($nodes as $node) {
            $this->ensure_node_is_visible($node);
            $node->click();
        }

        $choice = $this->fixStepArgument($choice);
        $nodes = $this->getSession()->getPage()->findAll('xpath',  '//button[@title[normalize-space(.) ="'.$label.'"]]/..//ul//li//a/span[text()[normalize-space(.) ="'.$choice.'"]]/..');
        if (count($nodes) == 0) {
            throw new ExpectationException('No option in select with text "' . $choice . '" found "', $this->getSession()->getDriver());
        }
        foreach ($nodes as $node) {
            $this->ensure_node_is_visible($node);
            $node->click();
        }
    }



}
