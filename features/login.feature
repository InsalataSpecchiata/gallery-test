Feature: Login
  In order to login
  As a gallery  user
  I need to be able to validate the username and password against portal



  @javascript
  Scenario: Login as non valid user
    Given I am on the homepage
    When I fill in "Username" with "demo2"
    And I fill in "Password" with "demo"
    And I press "Accedi"
    Then I should see "Credenziali non valide"

  @javascript
  Scenario: Login as user
    Given I am on the homepage
    When I fill in "Username" with "demo"
    And I fill in "Password" with "demo"
    And I press "Accedi"
    Then I should see "Gestione immagini"
    When I press "demo"
    Then I should see "Cambia password"
    Then I should see "Esci"

  @javascript
  Scenario: Login as user admin
    Given I am on the homepage
    When I fill in "Username" with "admin"
    And I fill in "Password" with "admin"
    And I press "Accedi"
    Then I should see "Gestione immagini"
    Then I should see "Gestione utenti"
    When I press "admin"
    Then I should see "Cambia password"
    Then I should see "Esci"
